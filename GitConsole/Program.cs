﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> messages = new List<string> { "Working with Git", "Is fun", " and easy" };

            foreach (string message in messages)
            {
                Console.WriteLine(message);
            }

            Console.ReadKey();
        }
    }
}
